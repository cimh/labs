
def Write(Mess):
    return print(Mess)

def WriteLn(Mess):
    return print(Mess + '\n')

def ReadStr(Mess):
    return input('Mess: ' % Mess)

def ReadInt(Mess):
    return int(input('Mess(int): ' % Mess))

def ReadFloat(Mess):
    return float(input('Mess(float): ' % Mess))
