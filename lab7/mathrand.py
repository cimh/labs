from random import *

def rnd(n1, n2):
    n = randint(n1, n2)
    return n

if __name__ == '__main__':
    print('n=%d' % rnd(0, 100))
