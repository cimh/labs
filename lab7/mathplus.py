

def summ(x, y):
    return x + y 

def pif(a, b):
    return a * a + b * b

def garm(n=10):
    s = 0
    for i in range(1, n + 1):
        s += i
    return s

def average(n=[]):
    summ = 0
    for i in n:
        summ += i
    return float(summ / len(n))

def digits(n):
    s = 0
    while n > 0:
        s += n % 10
        n = n // 10
    return s

if __name__ == '__main__':
    print('summ(23, 49): %d' % summ(23,49))
    print('pif(4, 5): %d' % pif(4, 5))
    print('garm(100): %s' % garm(100))
    print('average(1,3,9): %.2f' % average([1,3,9]))
    print('digits(789): %d' % digits(789))
